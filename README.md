## PURPOSE / WORKFLOW

1. Get datetimes and available seats of tours from "beyondak.com" and "alaskaexcursions.com"
2. Update datetimes and availability in Arctic Reservations system via API

## Technologies and Instruments

1. Languages: Python 3
2. Python Packages: pytz; pyvirtualdisplay; selenium
3. Virtual Display (Ubuntu): xvfb


## INSTALLATION

For Ubuntu:

1. `pip install -r requirements.txt`
2. Install Chrome:

    > wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 

    > sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

    > sudo apt-get update

    > sudo apt-get install google-chrome-stable

3. xvfb - creates Virtual Display, which allows to run GUI applications on a systems without monitor:

    > sudo apt-get install xvfb


## RUN SCRIPT

Via double-clicking:
    
    1. Activate virtual environment for this script
    2. Double-click "get_trips" file

Via python shell:
    
    1. Activate virtual environment for this script
    2. python3.* (enter to Python's shell)
    3. import get_trips
    4. get_trips.lets_run()

*(asteriks) - version of Python


## Architecture

1. In case if site does not have a required tour , notifications are sent to gmail
account ("email_notification" and "send_tour_problem_notification" functions in "utils.py").

2. As the script is going to be used on a server without monitor, virtual display must be used. For the purpose of
code simplifying web driver and virtual display are wrapped by "DriverAndDisplay" class.