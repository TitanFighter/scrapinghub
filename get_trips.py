import datetime
import pytz
import re

from selenium.common.exceptions import (NoSuchElementException, StaleElementReferenceException,
                                        TimeoutException, WebDriverException)

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait, Select

from time import sleep
from utils import DriverAndDisplay, send_tour_problem_notification


def update_trips_on_server(trips):
    """
    Get scraped trips and update Arctic Reservations system via API

    :param trips: Grabbed trips which must be sent to Arctic Reservations
    :type trips: {'trip_name1': {'schedules': {'free_seats': 10, 'trip_start_at': datetime.datetime}}, 'trip_name2': {}}
    
    :return: None
    """

    pass


def lets_run():
    required_tours = {ABAK: ['A TASTE OF NATURE KAYAK & GOURMET FOOD EXPERIENCE', 'MENDENHALL GLACIER TREK',
                             'FORTRESS OF THE BEARS KAYAK', 'PADDLE WITH WHALES KAYAK & PRIVATE BOAT ADVENTURE',
                             'GLACIER CANOE PADDLE & TREK', 'PRIVATE GLACIER KAYAK PADDLE & TREK',
                             'PRIVATE MENDENHALL GLACIER TREK'],

                      AlaskaX: ["Skagway Sled Dog and Musher's Camp", "Glacier Point ATV Exploration",
                                "Musher’s Camp and S’More Roasting"]}

    while True:
        for site_cls in required_tours:
            site = site_cls()

            trips = site.get_all_trips(required_tours[site_cls])
            update_trips_on_server(trips)


class ABAK:
    """
    Get times and available seats from https://beyond.com for provided tours.
    """

    basic_url = 'https://beyondak.com/'

    driver_n_display = None
    driver = None

    def __init__(self):
        self.driver_n_display = DriverAndDisplay(False)
        self.driver = self.driver_n_display.start()

    def get_all_trips(self, trip_names):
        """
        :param trip_names: Name of trips for which we need to get times and available seats.
        :type trip_names: (set or list or tuple) of strings
        
        :return: {'trip_name1': {'schedules': {'free_seats': 10, 'trip_start_at': datetime.datetime}}, 'trip_name2': {}}
        """

        if not any(type(trip_names) == t for t in [set, list, tuple]):
            raise TypeError('"trip_names" must be any of the next types: set, list, tuple')

        trips = {}

        now_date = datetime.datetime.now().date()
        current_year = now_date.year

        current_year_start_at_date = datetime.date(current_year, 4, 15)  # April 15
        current_year_finish_at_date = datetime.date(current_year, 10, 15)  # October 15

        try:
            self.driver.get(self.basic_url)
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.XPATH, '//article')))

            self.driver.find_element_by_class_name('section-title').location_once_scrolled_into_view

            next_slider_tours_xpath = '(//div[contains(@class, "tg-right-arrow") and ' \
                                      'contains(@class, "tg-nav-color") and ' \
                                      'contains(@class, "tg-nav-font")]/i)[1]'

            tours_n_urls = {}

            all_possible_tours = set()

            # Get tours urls from slider
            try:
                for i in range(5):
                    sleep(5)

                    all_tours = self.driver.find_elements_by_xpath('//article/div/div[2]/h2/a')

                    for tour in all_tours:
                        trip_url = tour.get_attribute("href")
                        trip_name = tour.text.upper()

                        all_possible_tours.add(trip_name)

                        if trip_name in trip_names:
                            tours_n_urls.setdefault(trip_name, trip_url)

                    self.driver.find_element_by_xpath(next_slider_tours_xpath).click()

            # Slider is not clickable anymore
            except WebDriverException:
                pass

            for trip_name, trip_url in tours_n_urls.items():
                self.driver.get(trip_url)

                # CLICK "BOOK THIS TRIP" button
                book_this_trip_btn_xpath = '//div[contains(@class, "xola-checkout") and ' \
                                           'contains(@class, "xola-custom") and contains(@class, "hero")]'
                WebDriverWait(self.driver, 30).until(
                    EC.presence_of_element_located((By.XPATH, book_this_trip_btn_xpath)))

                # Let page loader time to hide
                sleep(2)
                self.driver.find_element_by_xpath(book_this_trip_btn_xpath).click()

                # CALENDAR SECTION
                # Switch to iframe
                WebDriverWait(self.driver, 60).until(
                    EC.presence_of_element_located((By.ID, 'xola-multi-item-checkout-app')))
                calendar_iframe = self.driver.find_element_by_id('xola-multi-item-checkout-app')
                self.driver.switch_to.frame(calendar_iframe)

                # CLICK THE CLOSEST TRIP (By clicking we move calendar to the closest tour's date)
                # Some tours does not have this feature
                active_days_xpath = '//div[contains(@class, "datepicker") and contains(@class, "hasDatepicker")]' \
                                    '/div/table[@class="ui-datepicker-calendar"]/tbody/tr' \
                                    '/td[contains(@class, " ui-state-availability")]'

                try:
                    closest_trips_xpath = '//div[contains(@class, "datepicker") and ' \
                                          'contains(@class, "hasDatepicker")]/div/div[@class="upcoming-dates"]/ul/li[1]'
                    WebDriverWait(self.driver, 30).until(
                        EC.presence_of_element_located((By.XPATH, closest_trips_xpath)))
                    self.driver.find_element_by_xpath(closest_trips_xpath).click()

                    # Wait until trips data is loaded
                    WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.XPATH, active_days_xpath)))
                except TimeoutException:
                    pass

                months_checked = 0

                for i in range(9):  # 9 months (instead of "while True")
                    sleep(2)
                    active_days = self.driver.find_elements_by_xpath(active_days_xpath)

                    for day_num in range(len(active_days)):
                        day_linkable = active_days[day_num].find_element_by_xpath('./a')
                        day_linkable.click()

                        # Prevent "Message: stale element reference: element is not attached to the page document"
                        active_days = self.driver.find_elements_by_xpath(active_days_xpath)

                        # Months start from 0
                        trip_month = int(active_days[day_num].get_attribute("data-month")) + 1  # 0, 1, 2, 3, ...
                        trip_year = active_days[day_num].get_attribute("data-year")  # 2018

                        trip_day = active_days[day_num].find_element_by_xpath('./a').get_attribute('data-date')

                        if trip_day is None:
                            active_days = self.driver.find_elements_by_xpath(active_days_xpath)
                            sleep(2)
                            trip_day = active_days[day_num].find_element_by_xpath('./a').get_attribute('data-date')

                            if trip_day is None:
                                raise ValueError('trip_day is still None')

                        times_n_seats = \
                            self.driver.find_elements_by_xpath('//div[@class="arrival-time-btns-wrapper"]/div')

                        for time_n_seats in times_n_seats:
                            tries = 0

                            while True:
                                tries += 1

                                try:
                                    trip_time = \
                                        time_n_seats.find_element_by_xpath('./button/var[@timeslot]').text  # 13:00
                                    free_seats = \
                                        int(time_n_seats.find_element_by_xpath(
                                            './button/div[@class="slot-quantity "]/var').text)

                                    break

                                except StaleElementReferenceException:
                                    if tries == 2:
                                        raise StaleElementReferenceException()

                            if any(i in trip_time for i in ['AM', 'PM']):
                                datetime_format = '%m/%d/%Y %I:%M %p'
                            else:
                                datetime_format = '%m/%d/%Y %H:%M'

                            trip_start_at = str(trip_month) + '/' + trip_day + '/' + trip_year + ' ' + trip_time
                            trip_start_at = datetime.datetime.strptime(trip_start_at, datetime_format)
                            trip_start_at = trip_start_at.replace(tzinfo=pytz.timezone('US/Alaska'))

                            if current_year_start_at_date <= trip_start_at.date() <= current_year_finish_at_date:
                                trips.setdefault(trip_name, {'schedules': {}})
                                trips[trip_name]['schedules'].update(
                                    {trip_start_at: dict(free_seats=free_seats, trip_start_at=trip_start_at)}
                                )

                    if not active_days:
                        months_checked += 1

                    if months_checked == 5:  # Gap between January and May
                        self.driver.switch_to.default_content()
                        break

                    next_month_btn_xpath = \
                        '//span[contains(@class, "xola-icon") and contains(text(), "rightcaret")]'
                    self.driver.find_element_by_xpath(next_month_btn_xpath).click()

        except Exception as err:
            self.driver_n_display.stop()
            raise Exception(err)

        self.driver_n_display.stop()

        # Check if we did not miss any tour
        send_tour_problem_notification('ABAK', trip_names, trips, all_possible_tours)

        return trips


class AlaskaX:
    """
    Get times and available seats from https://www.alaskaexcursions.com
    """

    basic_url = 'https://www.alaskaexcursions.com/admin/booking/find_available_excursions'

    driver_n_display = None
    driver = None

    def __init__(self):
        self.driver_n_display = DriverAndDisplay()
        self.driver = self.driver_n_display.start()

    def get_all_trips(self, trip_names):
        """
        :param trip_names: Name of trips for which we need to get times and available seats.
        :type trip_names: (set or list or tuple) of strings
        
        :return: {'trip_name1': {'schedules': {'free_seats': 10, 'trip_start_at': datetime.datetime}}, 'trip_name2': {}}
        """

        if not any(type(trip_names) == t for t in [set, list, tuple]):
            raise TypeError('"trip_names" must be any of the next types: set, list, tuple')

        # HANDLE TRIPS
        trips = {}

        now_date = datetime.datetime.now().date()
        current_year = now_date.year

        start_at_date = datetime.date(current_year, 4, 15)  # April 15
        finish_at_date = datetime.date(current_year, 10, 15)  # October 15

        # No reason to check trips after "finish_at_date", because season is closed -> no trips anymore
        if now_date >= finish_at_date:
            return trips

        # If current day is latter than April 15, then set start_at_date as today, instead of April 15
        if now_date > start_at_date:
            start_at_date = now_date

        days_range = (finish_at_date - start_at_date).days + 1  # We need to include October 15

        # 1 shows available seats from 1 to 5
        # 6 shows available seats from 6 to 10
        # 11 shows available seats from 11 to 15
        # 16 (15 for adults + 1 for kid) shows available seats from 16 to 20
        # 21 (15 for adults + 6 for kid) shows available seats from 21 to 25 (actually 24, as bus has just 24 seats)
        guests_input = [1, 6, 11, 16, 21]

        # 1st PAGE
        get_tries = 0

        while True:
            try:
                self.driver.get(self.basic_url)
                break
            except TimeoutException:
                get_tries += 1
                sleep(10)

            if get_tries == 5:
                raise TimeoutException()

        next_page_btn_xpath = "//fieldset/button[@type='submit']"  # on the first page
        trips_list_xpath = "//fieldset/ol"  # on the second page

        for select_date in range(0, days_range):
            date_to_input = (start_at_date + datetime.timedelta(select_date)).isoformat()  # 2016-06-30
            tries = 0

            while True:
                total_trips_this_day = 0
                total_today_trips_saved = 0
                today_trips = {}

                try:
                    no_trips = False

                    # SET TRIP DATE
                    WebDriverWait(self.driver, 15).until(EC.presence_of_element_located((By.NAME, 'excursion_date')))

                    date_field = self.driver.find_element(by='name', value='excursion_date')
                    date_field.clear()
                    sleep(0.2)

                    date_field.send_keys(date_to_input)
                    sleep(0.5)

                    for guest_num in guests_input:
                        # Why 15? 15 is maximum selection available for adults
                        if guest_num <= 15:
                            kids_num = 0
                        else:
                            kids_num = guest_num % 15
                            guest_num = 15

                        # SET NUM OF ADULTS
                        WebDriverWait(self.driver, 15).until(
                            EC.presence_of_element_located((By.NAME, 'guests[adults]')))

                        adults_select = Select(self.driver.find_element(by='name', value='guests[adults]'))
                        adults_select.select_by_index(guest_num)
                        sleep(0.5)

                        # SET NUM KIDS.
                        # Also sets kids to 0, when we go back to initial page from the 2nd page (with data trips)
                        WebDriverWait(self.driver, 15).until(
                            EC.presence_of_element_located((By.NAME, 'guests[kids]')))

                        adults_select = Select(self.driver.find_element(by='name', value='guests[kids]'))
                        adults_select.select_by_index(kids_num)
                        sleep(0.5)

                        # CLICK "NEXT" BUTTON
                        next_page_btn = self.driver.find_element_by_xpath(next_page_btn_xpath)
                        next_page_btn.send_keys(Keys.RETURN)
                        # END of THE 1st PAGE

                        # 2nd PAGE
                        # Try to get list of trips schedules
                        try:
                            WebDriverWait(self.driver, 3).until(
                                EC.presence_of_element_located((By.XPATH, trips_list_xpath)))

                            html_trips = self.driver.find_element_by_xpath(trips_list_xpath)
                            trips_lists = html_trips.find_elements_by_tag_name("li")

                            saved_trips_from_this_page = 0

                            for trip in trips_lists:
                                if guest_num == 1:
                                    total_trips_this_day += 1

                                trip_name = trip.find_element_by_class_name('excursion_booking_name').text
                                trip_date = trip.find_element_by_class_name('excursion_booking_date').text  # 05/01/2018
                                trip_time = trip.find_element_by_class_name('excursion_booking_time').text  # 10:00

                                trip_start_at = trip_date + ' ' + trip_time
                                trip_start_at = datetime.datetime.strptime(trip_start_at, '%m/%d/%Y %H:%M')
                                trip_start_at = trip_start_at.replace(tzinfo=pytz.timezone('US/Alaska'))

                                try:
                                    free_seats = \
                                        trip.find_element_by_class_name('excursion_booking_low_availibity').text
                                    free_seats = int(re.findall(r'\d+', free_seats)[0])
                                except NoSuchElementException:
                                    continue

                                # There are cases when AlaskaX has identical trips with same datetime (duplications).
                                # For this reason 'trip_start_at' is used as a key instead of 'trip_id'
                                trips.setdefault(trip_name, {'schedules': {}})
                                trips[trip_name]['schedules'].update(
                                    {trip_start_at: dict(free_seats=free_seats, trip_start_at=trip_start_at)}
                                )

                                today_trips.setdefault(trip_name, {'schedules': {}})
                                today_trips[trip_name]['schedules'].update(
                                    {trip_start_at: dict(free_seats=free_seats, trip_start_at=trip_start_at)}
                                )

                                total_today_trips_saved += 1
                                saved_trips_from_this_page += 1

                            self.driver.execute_script("window.history.go(-1)")  # Go back to the previous page

                            # If the current page have info about availability for all trips, then no reason to check
                            # next page, because next page is empty and we waste 15 seconds for WebDriverWait
                            if saved_trips_from_this_page == len(trips_lists):
                                break  # break 'guest_num', and go to 'select_date'

                        # Except no page with trips schedules
                        except TimeoutException:
                            try:
                                msg_xpath = '//div/p/a'
                                WebDriverWait(self.driver, 15).until(
                                    EC.presence_of_element_located((By.XPATH, msg_xpath)))

                                if self.driver.find_element_by_xpath(msg_xpath).text == \
                                        'Press the back button on your browser and ' \
                                        'select a different date and/or excursion':

                                    self.driver.execute_script("window.history.go(-1)")  # Go back to the previous page

                                    # break 'guest_num', then the break below breaks 'while'.
                                    # As a result we go to the next date.
                                    no_trips = True
                                    break

                            except TimeoutException:
                                sleep(2)
                                self.driver_n_display.stop()
                                raise ValueError('We are expecting to be on the page which does not have trips and '
                                                 'propose to go back to the previous page, but for some reason '
                                                 'it seems that we are on the different page or see other text.'
                                                 '\nDate is: %s'
                                                 '\nGuest num: %s' % (date_to_input, guest_num))

                    # Check if all trips are collected OR current date have no trips
                    if (total_trips_this_day == total_today_trips_saved) or no_trips:
                        break  # break While
                    else:
                        tries += 1

                except TimeoutException:
                    tries += 1
                    sleep(10)

                    get_tries = 0

                    while True:
                        try:
                            self.driver.get(self.basic_url)
                            break
                        except TimeoutException:
                            get_tries += 1
                            sleep(10)

                        if get_tries == 5:
                            raise TimeoutException()

                if tries == 5:
                    self.driver_n_display.stop()

                    # Check if all trips are collected
                    if total_trips_this_day != total_today_trips_saved:
                        self.driver_n_display.stop()
                        raise ValueError("There are %s trips on %s, but just %s have been saved"
                                         "\nToday's trips: %s" %
                                         (total_trips_this_day, date_to_input, total_today_trips_saved, today_trips))

                    else:
                        raise TimeoutException()

        self.driver_n_display.stop()

        # Check if we did not miss any tour
        send_tour_problem_notification('AlaskaX', trip_names, trips, list(trips))

        return trips


# Executes the code automatically if this file is launched for example via command line or by double-click
if __name__ == "__main__":
    lets_run()
