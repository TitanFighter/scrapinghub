import os
import shutil
import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from time import sleep


def email_notification(subject_msg=None, error_msg=None):
    """
    Prepare email with error which must be sent to Gmail account.

    :param subject_msg: Subject
    :param error_msg: Body
    
    :return: None
    """

    gmail_user = 'some_user@gmail.com'
    gmail_pwd = 'some_password'

    FROM = gmail_user
    TO = [FROM]

    msg = MIMEMultipart('alternative')
    msg.set_charset('utf8')
    msg['From'] = FROM
    msg['To'] = ', '.join(TO)
    msg['Subject'] = subject_msg
    msg.attach(MIMEText(error_msg, 'plain', 'UTF-8'))

    tries = 0

    while True:
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(gmail_user, gmail_pwd)
            server.sendmail(FROM, TO, msg.as_string())
            server.close()

            break

        except smtplib.SMTPServerDisconnected:
            if tries == 5:
                raise smtplib.SMTPServerDisconnected('Notification can not be sent withing 5 mins')
            else:
                sleep(60)
                tries += 1

        except smtplib.SMTPAuthenticationError:
            print('subject_msg: %s'
                  '\n\nerror_msg: %s' % (subject_msg, error_msg))

            break


def send_tour_problem_notification(site_name,
                                   required_tours,
                                   trips_grabbed,
                                   possible_names):
    """
    1. Check if all tours from "required_tours" are in "trips_grabbed".
       If tours are not in "trips_grabbed", it can mean that tour name on a source-site is changed.

    2. Check if tours on source-site have any scheduled trips (have any times).

    :param site_name: Name of site from which data was scrapped.
    :type site_name: str

    :param required_tours: Name of tours, times and available seats of which we should scrap.
    :type required_tours: (set or list or tuple) of strings

    :param trips_grabbed: Dict of tours with times and available seats which were scrapped.
    :type trips_grabbed: {'trip_name1': {'schedules': {'free_seats': 10, 'trip_start_at': datetime.datetime}}}

    :param possible_names: List of all tours, which the source-site has.
    :type possible_names: (set or list or tuple) of strings

    :return: None
    """

    for tour_name in required_tours:
        if tour_name not in trips_grabbed:
            notification_subject = site_name.upper() + ' probably has changed a tour name'
            notification_msg = 'The tour "%s" can not be found in "%s" system' \
                               '\n\nPossible values are: %s' \
                               '\n\nPlease provide corrent name from some values above.' \
                               % (tour_name, site_name.capitalize(), possible_names)

            email_notification(notification_subject, notification_msg)

        elif not trips_grabbed[tour_name]['schedules']:
            notification_subject = '%s does not have any times for the tour "%s"' % (site_name.upper(), tour_name)
            notification_msg = notification_subject

            email_notification(notification_subject, notification_msg)


class DriverAndDisplay:
    """
    Wrapper for web driver and virtual display.
    """

    driver = None
    display = None

    def __init__(self, need_virtual_display=True, show_virtual_display=False):
        """
        :param need_virtual_display: - set to 'True' if it is necessary to run script on server or other machine
                                       without monitor

                                     - if 'False', developer will see native system
                                       browser (not a browser inside 'Display') on his\her own monitor

        :param show_virtual_display: - if 'need_virtual_display = True', when 'show_virtual_display=True',
                                       developer can see 'virtual' display on his\her own
                                       monitor (usually for debug purpose)

                                     - if 'need_virtual_display = False',
                                       developer will not see anything (production mode)
        """

        self.need_virtual_display = need_virtual_display
        self.show_virtual_display = show_virtual_display

    def start(self):
        """
        Launch web driver and virtual display (if needed).

        :return: <class 'selenium.webdriver.chrome.webdriver.WebDriver'>
        """

        if self.need_virtual_display:
            self.display = Display(visible=self.show_virtual_display, size=[1024, 768])  # width x height
            self.display.start()

        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")

        chrome_driver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'chromedriver')

        self.driver = webdriver.Chrome(chrome_driver_path, chrome_options=chrome_options)
        self.driver.set_window_size(1024, 768)  # width x height
        self.driver.set_page_load_timeout(60)

        return self.driver

    def stop(self):
        """
        Stops web driver and display + remove temporary files (if exist).

        :return: None
        """

        self.driver.quit()

        if self.display:
            try:
                self.display.stop()
            except KeyError:  # KeyError: 'DISPLAY'
                pass

        # Selenium has a bug that on some systems diver.quit() does not remove temporary files
        get_dirs = [x[0] for x in os.walk('/tmp')]
        for get_full_path_dir in get_dirs:
            if 'chrom' in get_full_path_dir.lower():  # 'chrom' covers 'chrome' and 'chromium', so do not change
                shutil.rmtree(get_full_path_dir, ignore_errors=True)
